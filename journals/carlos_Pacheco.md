## Carlos's Journal

### Contributions

## 01/16/24
While sharing my screen i did the following

* collaborated with group as driver while andre was navigating
* Did the inital set up and created the tables

## 01/17/24
As a driver I did the following:

* Set up pg-admin

## 01/18/24
As navigator I assisted in the following:

* Assisted in getting the login, logout, create account endpoints
* Ran into the following roadblock: kept getting a "json not serializable error" for my date
  data type in the accounts table. 

## 01/19/24
As navigator I assisted in the following:
  * Assisted in creating the appointments table and setting up a getall appointments function.

As a driver I:
  * Completed the create function for appointents
  * During this we ran into a problem. I could not create an appointment because at the time the appointment's table had a patient/hcp foreign key. We kept getting an error because we were trying to reference patients/hcps when there werent any. What we needed was for a hcp or patient account to be created once an account was created but after days of bumping our heads we decided it was best to get rid of the patients/hcp table and just use accounts. The two tables had pretty much the same data so all we did was simplify our database. 

## 01/22/24
As a driver I:
  * Added the update functionality to the appointments table. 
  * I also made the id column in appoinmtments table reference the id of the accounts table. 

## 01/23/24
As a navigator I:
  * Assisted in creating the CRUD functionality for our feelings table. 
  * Along with altering the feeling's table columns

  challenge:
   * Ayden was having issues pushing from local to his gitlab branch we tried committing/staging changes and were not succesful. 3 siers came into our room but we could not figure out the problem. We all decided that it was best to start over and clone from main. We then manually transfered the files need and succesfully pushed his local to a newly created gitlab branch. 

## 01/24/24
As a navigator I:
  * Assisted Andred in setting up our Open AI endpoint

## 01/25/24
As a Driver I:
  * Refactored the following tables in order to allow for better referencing:
  feelings, appointments, summaries. 
  * I also implemented a ristriction on doctors so that they can not create feelings. Only patients can create feelings. 

## 01/26/24
As a navigator I:
  * Assisted Andre in restricting user access to certain endpoints depending on their role. 

## 01/29/24
As a driver I:
  * initiated the set up for the front end and started the signup page

## 01/30/24
As a navigator:
  * I assisted in getting the front-end authorization up and running. We where only able to get the login to work but not the redirect after login. 

## 01/31/24
As a driver I:
  * implemented the logout and login features for a single user role only. 
roadblock
  * we got stuck today whenever we tried navigating the logged in user to route depending on which role they were. We've tried several things like moving around if statements in the loginForm.jsx file. Seirs were in our rooms all day today but we still could not find the solution. We will be reaching out to an instructor tommorow.

## 02/01/24
  * Today we worked on getting the signup page logic to work. We were not able to complete this functionality but will meet up tommorow on our day off. 
  * I assisted the team in creating an appointment list page. 

## 02/05/24
  As a driver I:
  * completed the create appointment form. I also added functionality to the cancel and schedule buttons on the appointment list page. Whenever a patient creates an appointment they are navigated to the appointments list. 
  * Added create appointment button. 

## 02/06/24
  As a driver I:
  * created an add that is dynamically rendered based on if there are any feelings or not.  
  * Restructured the appointlist page so that a no appointments found message is displayed when there are no appointments available.

 ## 02/07/24
  As a driver I:
  * Created a unit test for the create appointment endpoint. 
  * This gave us a hard time so we spent most of our day watching Delonte's video on unit tests. 

## 02/08/24
  As a driver I:
  * fixed navigation links for the logo image
  * removed shedule button when a user is a patient
  * removed add apointment button when a user is an hcp
  * added diagram and model to readme