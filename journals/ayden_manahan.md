## Ayden's Journal

### Contributions

## 01/16/24
- Watched while Andre set up group and merged vite to main branch.
- Created my own branch named Ayden
- Cloned, Forked and Merged from main.
- Pair programming with group, Carlos lead - We created postgres database and database tables

## 01/17/24
- Today as a group we set up our back and frontend Authourization using JWTdown. Andre drove and we all watched for errors.
- Overwatched carlos and helped confirm changes to database tables.
- Broke off into a separate branch ayden_auth to try and fix/complete authorization

## 01/18/24
- Continued to fix our authorization
- Birthdate was causing issues with auth (of type not JSON serializable)
- Removed birthdate from database and all models that contained birthdate
- Succesfully finished Auth functionality within FastAPI
- Decided as a group to restructure our database tables to better fit our needs for scope and longevity of the project.

## 01/19/24
I drove the coding today
- Added appointments table to our database
- Added feelings table to database
- Created Error and appointment In and Out models
- Created appointment queries to get all appointments and order them by date
- Watched as Andre added some routes to our Project

## 01/22/24
Carlos was driving today.
- Mob Programming, trying to add the PUT/Update method to our appointment model.
- Complete Update method finishing our CRUD operations for Appointments

## 01/23/24
I drove the coding today
- Going to work on the CRUD for our feelings model
- Created routes for feelings models
- Created feelings models
- Created ..... function for feelings

## 01/24/24
- Reshaped our Database tables
- Talked about how we would want to implement open a.i. into our project

## 01/25/24
Andre and Carlos Drove I believe
- Read open ai docs
- Worked on implementing ai into our project
- Created backend endpoints for a.i.
- Did some error handling


01/26/24
Pretty chill day, Carlos was busy until after lunch, Cameron was sick.
- Andre merged to main.
- Joined a live share collaboration session with Andre and Carlos to fix code with flake8 and black errors.

## 01/29/24
Mob coding with live share
- Started to develop our front end
- Added the auth page to our front end

## 01/30/24
- Looked over Andres code
- Watched as Andre merged auth pg to main

I drove most of the coding today
    - I created our apiSlice, querySlice, and store js files in our app folder within our ghi.
    - Created two endpoints for apiSlice for getting all appointments and getting the token to see if a user is logged in
    - Did lots of error handling
    - Connected front and backend auth together ( not fully functional running into errors )


## 02/01/24
Worked on our login pgs Mob coding
- Refetching our data and making sure we can log in sign up and create account on the front end, and making the front and back-end connect

## 02/05/24
Practice Exam this morning until after lunch
Working on all of our pgs today for the front end, Mob coding.
- Watched as Carlos worked on front end

## 02/06/24
Drove most of the code for this after noon
- Created a way for and HCP to see a list of all of their patients on the first end
- Created a way for an HCP to filter their patients by first and last name.
- Reformatted code to comply with flake 8, prettier, and black
- Merged to main and set Carlos as reviewer.
- Pushed Journals to main.

## 02/07/24
All 4 of us drove at one point today
- Worked on creating our unit tests for our application
- My unit test is close to passing i hope to complete and push that tonight or early morning
- I am creating the unit test to get all patient accounts
but may switch to a create patient account.
- I also plan on pushing my journals tonight
- Reading through CI and CD as well to prepare for tomorrow.

## 02/08/24
Drove code in the morning for a littl bit
- completed unit test to get an appointment
- learned it needs to corolate to the specific query and router and model
- finished unit test to get one appointment
- All unit tests passed and working
- spent the rest of the day working on css as a group
carlos cameron and andre drove during the day
