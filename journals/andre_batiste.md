<div align="center">

# Andre
_Daily Journal_
</div>

# Contributions

### 01/16/24
- While sharing my screen I did the following
    * forked inital project and merged Vite
- while carlos was driving I was the navigator during the project setup(Docker and linking the front end to db)
##

### 01/17/24
- While sharing my screen i did the following
    * I was driving during the Auth coding process
    * did not finish the auth set up(was stuck on create account)
##

### 01/18/24
- While sharing my screen i did the following
    * finished Auth and figured out create account
- While Cameron was driving I was the navigator(adding roles and converting datetime to string)
    * figured out datetime converson and roles for database
    * figured out datetime converson and roles for database
##

### 01/19/24
- I was the navigator while Ayden was driving(creating CRUD routes and Models for appointments)
- I was the navigator while Carlos was driving(creating queries for appointments)

### Thoughts

>Had some trouble with linking appointments to doctors and patients. but we ended up reformatting how our database tables were linked and got it working.
##

### 01/22/24
- I was the navigator while Cameron was driving for the creation of queries and routes for appointments

##

### 01/23/24
- I was the navigator while Ayden was driving for creation of queries, routes, and Models for feelings

##

### 01/24/24
- I was the driver for getting the api key, creating the route for our OpenAI api

### Thoughts

>Had some trouble with getting the api to work correctly but with help from Stesha and Bart we were able to get the errors broken down and see what mistake we were making in the code.
##

### 01/25/24
- I was the navigator for refactoring db tables and restricting appointment booking/creation to patients only.
##

### 01/26/24
- restricted all endpoints based on current user and the user's role

### Thoughts

>
##

### 01/29/24
- added routes we may use
- Carlos started the signup form
- I finished the signup form

### Thoughts

>
##

### 01/30/24
- added css file to go with each page and styled the css for the pages
- removed unused links and components from the app.jsx file
- added the correct routes


### Thoughts

> We changed what
##

### 01/31/24
- i drove while working on frontend auth(login)
-


### Thoughts

>login was partially working there were issues with the handle effect
##

### 02/1/24
- I drove while adding the feelings list and hcp list
- i drove while adding the patient nav
- carlos fixed the login functionality

### Thoughts

>
##


### 02/5/24
- Added cancel and schedule buttons to hcp appointment list
- I dove while adding sign-up functionality
-  Carlos drove while adding create appointment functionality
- I drove while removing passwords from database and leaving the hashed password in the database



### 02/6/24
- Ayden aded patient list and search functionality for patient list
- fixed the links in the nav bars
- Added the correct links to navigate a patient or hcp to the correct place when they login


### 02/7/24
- Added the Readme for our project with docs for running it and what each endpoint does
- restructured pages and fixed format errors
- added and changed styling to pages


### 02/8/24
- completed the project
- worked on completeing unit test
- Our over all project is done and we researched and tried our stretch goals.

##

### 02/9/24
- Working on getting project ready to present by going throuh every page and checking for prints/console
logs and general testing of the application

### Thoughts

>everything looks good we had a few small changes to make.
##