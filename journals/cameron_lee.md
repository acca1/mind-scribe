## Cam's Journal

## 1/16/24
- Cloned, Forked, Merged from main
- Collaborated on code, Carlos was Driving
- Looked over docs on postgres @ https://www.postgresqltutorial.com/

## 1/17/24
-Collaborated on code, Andre was driving
-Theorized how to pursue making an auth token after watching learn lessons
-Ran out of time and got a decent dent of work done as a team
-looked at pydantic docs and reviewed fields and models

## 1/18/24
-Found out there's another video that goes over postgres for auth to help with query, we
were watching the mongo ones and trying to implement it with reverse engineering
-Watched video together and edited code to properly line up with what is needed
-Collaborated on code, Andre was driving; Got Auth working; removed birthdate, possibly
re-implement later when we find out how to easily stringify it for json (apparently it is
supposed to auto-stringify and be readable, but it isn't working correctly)
-Collaborated on more code, Drove and had team help on creating the rest of our db_tables
for postgres

## 1/19/24
-Worked on postgres tables and crud implementations, Ayden driving
-Andre figured out how to convert date for JSON to read it properly and implemented it in
-Switched up database to remove redundant information and make linking easier in postgres
-Carlos drove and we made the create appointment functionality for our app

## 1/22/24
-Worked on updating an appointment, Carlos was driving
-Drove and added delete and get one functionality to Appointments

## 1/23/24
-Ayden drove, completed CRUD for feelings
-Fixed Feelings table as well

## 1/24/24
-Collaborated on getting openai API key to work on backend for querying purposes
-Read over API documentation, ran into a problem with it using an "old version" of doing things, tried downgrading but did not work, googled and watched videos to figure out how to use the newer version
-Got API working, Andre driving, stackoverflow and docs to get it working

## 1/25/24
-Worked on revising tables to create a Summary table correctly for openai to summarize details of a couple of days' worth of feelings description
-went over postgresql docs on partitioning tables and available options for looping through
-decided to do the logic within the query instead to parse the data needed

## 1/26/24
-Did not attend

## 1/29/24
-

## 1/30/24
-Worked on getting login auth to work, Andre driving
-Watched videos and reviewed docs on how to implement
-Stayed late to get it wrapped up, got everything up
and running by the end

## 1/31/24
-Worked on getting logout to work, Carlos driving
-Watched videos and reviewed docs on how to implement
-Got login to reroute to either the patient or HCP homepage; works
90% of the time
-Had a lot of errors with caching holding the key longer than we
wanted, still working towards a solution, refactored code breaking
it down but ended up doing a giant circle and codebase ended up
basically the same with small tweaks
-Stayed late again but weren't able to make the progress we wanted

## 2/1/24
-Worked on getting login/logout to work properly again
-Worked on making the appointment list parse data through and making
the elements "reactive"
-Worked on the Nav components

## 2/5/24
-Worked on signup form
-Refactored excess pages out
-Drove and worked on logout function and appointment list
-Didn't finish getting logout to work; database broke

## 2/6/24
-Drove and worked on signout function, got it working and rerouting to main page
-Ayden drove, collaborated on patient list and added a filter by name function
Refactored code to remove prints, prettier, etc.
-Drove and added create feelings with slice

## 2/7/24
-Created a unit test to mock creation of a feeling by a patient
-Got unit test working
-Collaborated on some code revision and cleanup; getting ready to deploy tomorrow
-Looked at some possible backgrounds for our pages

## 2/8/24
-Attempted to create an upload for users to use for profile-pictures.
-Tried various different techniques and watched multiple videos, attempted
to piece various things together for a solution, didnt quite make it to the end
-Will pick up tomorrow where I left off, looking good, have the pictures breaking
down into bytes and gonna put them back together with PIL, hopefully
can get the data easily afterwards