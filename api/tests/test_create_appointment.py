from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.appointments import AppointmentQueries
from models.appointments import AppointmentOut

client = TestClient(app)


def fake_user():
    return {"id": 1, "username": "username", "role": "patient"}


apt = {
    "hcp_id": 1,
    "appt_date": "06-30-1998",
    "appt_time": "10:00am",
    "appt_reason": "checkup",
    "status": "requested",
}


class MockAppointmentQueries:
    def create(
        self,
        apt,
        patient_id: int,
    ):
        return AppointmentOut(
            id=1,
            patient_first_name="John",
            patient_last_name="Doe",
            patient_birthdate="1998-06-30",
            hcp_first_name="Carlos",
            hcp_last_name="Pacheco",
            patient_id=patient_id,
            hcp_id=apt.hcp_id,
            appt_date=apt.appt_date,
            appt_time=apt.appt_time,
            appt_reason=apt.appt_reason,
            status=apt.status,
        )


def test_create():
    # Arrange
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_user
    app.dependency_overrides[AppointmentQueries] = MockAppointmentQueries

    response = client.post("/api/appointments", json=apt)

    # Assert
    assert response.status_code == 200
    aptOut = response.json()
    assert len(aptOut) == 12
    assert response.json() == {
        "id": 1,
        "patient_first_name": "John",
        "patient_last_name": "Doe",
        "patient_birthdate": "1998-06-30",
        "hcp_first_name": "Carlos",
        "hcp_last_name": "Pacheco",
        "patient_id": 1,
        "hcp_id": 1,
        "appt_date": "1998-06-30",
        "appt_time": "10:00am",
        "appt_reason": "checkup",
        "status": "requested",
    }

    app.dependency_overrides = {}
