from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.feelings import FeelingsQueries, FeelingsOut

client = TestClient(app)


def fake_account():
    return {"id": 1, "username": "username", "role": "patient"}


class fakeQuery:
    def get_one(self, id: int, patient_id: int):
        return FeelingsOut(
            id=id,
            patient=patient_id,
            name="Happy",
            date="02-07-2024",
            description="im happy",
            emoji="happy.png",
        )


def test_get_feeling():
    # Arrange
    id = 1
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_account
    app.dependency_overrides[FeelingsQueries] = fakeQuery

    # Act
    response = client.get(f"/api/feelings/{id}")

    # Assert
    assert response.status_code == 200
    feelingsOut = response.json()
    assert len(feelingsOut) == 6
    assert response.json() == {
        "id": 1,
        "patient": 1,
        "name": "Happy",
        "date": "02-07-2024",
        "description": "im happy",
        "emoji": "happy.png",
    }

    # Cleanup
    app.dependency_overrides.clear()
