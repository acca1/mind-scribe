from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.appointments import AppointmentQueries, AppointmentOut

client = TestClient(app)


def fake_account():
    return {"id": 1, "username": "username", "role": "patient"}


id = 1


class fakeQuery:
    def get_one(self, id: int, patient_id=None, hcp_id=None):
        return AppointmentOut(
            id=id,
            patient_first_name="Ayden",
            patient_last_name="Manahan",
            patient_birthdate="02-14-2002",
            hcp_first_name="Gordon",
            hcp_last_name="Ramsey",
            patient_id=patient_id,
            hcp_id=1,
            appt_date="2024-02-07",
            appt_time="07:30",
            appt_reason="Help",
            status="Approved",
        )


def test_get_appointment():
    # Arrange
    id = 1
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_account
    app.dependency_overrides[AppointmentQueries] = fakeQuery

    # Act
    response = client.get(f"/api/appointments/{id}/")

    # Assert
    assert response.status_code == 200
    appointmentOut = response.json()
    assert len(appointmentOut) == 12
    assert response.json() == {
        "id": 1,
        "patient_first_name": "Ayden",
        "patient_last_name": "Manahan",
        "patient_birthdate": "02-14-2002",
        "hcp_first_name": "Gordon",
        "hcp_last_name": "Ramsey",
        "patient_id": 1,
        "hcp_id": 1,
        "appt_date": "2024-02-07",
        "appt_time": "07:30",
        "appt_reason": "Help",
        "status": "Approved",
    }
    # Cleanup
    app.dependency_overrides.clear()
