from authenticator import authenticator
from fastapi.testclient import TestClient
from main import app
from queries.feelings import FeelingsQueries, FeelingsOut

client = TestClient(app)


def fake_patient():
    return {"id": 1, "username": "username", "role": "patient"}


feelings = {"description": "Feeling good", "emoji": "happy.jpg"}


class TestFeelingsQueries:
    def create(self, feelings, patient=None):
        return FeelingsOut(
            id=1,
            patient=patient,
            name="Happy",
            date="02/07/2024",
            description=feelings.description,
            emoji=feelings.emoji,
        )


def test_fake_create_feelings():
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_patient

    app.dependency_overrides[FeelingsQueries] = TestFeelingsQueries

    response = client.post("/api/feelings", json=feelings)

    assert response.status_code == 200
    feelingsOut = response.json()
    assert len(feelingsOut) == 6
    assert response.json() == {
        "id": 1,
        "patient": 1,
        "name": "Happy",
        "date": "02/07/2024",
        "description": "Feeling good",
        "emoji": "happy.jpg",
    }

    app.dependency_overrides = {}
