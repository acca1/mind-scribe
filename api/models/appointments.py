from pydantic import BaseModel, validator
from datetime import datetime, date
from typing import Optional


class Error(BaseModel):
    message: str


class AppointmentIn(BaseModel):
    hcp_id: Optional[int]
    appt_date: Optional[str]
    appt_time: Optional[str]
    appt_reason: Optional[str]
    status: str = "requested"

    @validator("appt_date")
    def parse_dob(cls, v):
        return datetime.strptime(v, "%m-%d-%Y").date()


class HttpError(BaseModel):
    detail: str


class AppointmentOut(BaseModel):
    id: int
    patient_first_name: str
    patient_last_name: str
    patient_birthdate: str
    hcp_first_name: str
    hcp_last_name: str
    patient_id: int
    hcp_id: int
    appt_date: date
    appt_time: str
    appt_reason: str
    status: str


class UpdateAptResponse(BaseModel):
    success: bool
    message: str
