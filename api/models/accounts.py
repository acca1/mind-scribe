from pydantic import BaseModel, validator
from jwtdown_fastapi.authentication import Token
from datetime import datetime


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    first_name: str
    last_name: str
    birthdate: str
    profile_picture: str
    password: str
    company: str
    address: str
    role: str

    @validator("birthdate")
    def parse_dob(cls, v):
        return datetime.strptime(v, "%m-%d-%Y").date()


class AccountOut(BaseModel):
    id: int
    username: str
    first_name: str
    last_name: str
    birthdate: str
    profile_picture: str
    address: str
    company: str
    role: str


class AccountOutWithHashedPassword(AccountOut):
    hashed_password: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut
