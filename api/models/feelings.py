from pydantic import BaseModel
from datetime import datetime
import os


class FeelingsIn(BaseModel):
    emoji: str
    description: str


class HttpError(BaseModel):
    detail: str


class FeelingsOut(BaseModel):
    id: int
    patient: int
    name: str
    date: str
    description: str
    emoji: str

    def __init__(self, **data):
        if "date" not in data:
            data["date"] = datetime.now().strftime("%m-%d-%Y")
        super().__init__(**data)
        self.name = self.extract_name_from_filename(data["emoji"])

    @staticmethod
    def extract_name_from_filename(filename: str) -> str:
        name, _ = os.path.splitext(filename)
        return name.capitalize()


class UpdateFeelingsResponse(BaseModel):
    success: bool
    message: str
