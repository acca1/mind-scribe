from fastapi import APIRouter, Depends, HTTPException, status
from authenticator import authenticator

from typing import List, Optional
from models.feelings import UpdateFeelingsResponse, HttpError, FeelingsIn
from queries.feelings import (
    FeelingsOut,
    FeelingsQueries,
)

router = APIRouter()


@router.post("/api/feelings", response_model=FeelingsOut | HttpError)
async def create_feelings(
    feelings: FeelingsIn,
    account_data: dict = Depends(authenticator.try_get_current_account_data),
    repo: FeelingsQueries = Depends(),
):
    if account_data:
        usertype = account_data.get("role")
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Not authenticated",
        )
    if usertype == "patient":
        patient = account_data.get("id")
        if patient:
            create = repo.create(feelings, patient)
            return create
        else:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Not authenticated",
            )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You cannot create a feeling as an HCP.\
 Log in as a patient in order to create a feeling",
        )


@router.get(
    "/api/feelings/{id}",
    response_model=Optional[FeelingsOut] | HttpError,
)
async def get_feelings(
    id: int,
    current_user: dict = Depends(authenticator.try_get_current_account_data),
    repo: FeelingsQueries = Depends(),
) -> FeelingsOut:
    if not current_user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Not authenticated",
        )
    else:
        role = current_user.get("role")
        feelings = None

        if role == "hcp":
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="You cannot view a feelings as an HCP.",
            )
        elif role == "patient":
            patient_id = current_user.get("id")
            feelings = repo.get_one(id, patient_id)
    if feelings is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No Feelings found",
        )

    return feelings


@router.get("/api/feelings", response_model=List[FeelingsOut] | HttpError)
async def get_all_feelings(
    repo: FeelingsQueries = Depends(),
    current_user: dict = Depends(authenticator.try_get_current_account_data),
):
    if current_user:
        role = current_user.get("role")

        if role == "hcp":
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="You cannot view feelings as an HCP.",
            )
        elif role == "patient":
            patient_id = current_user.get("id")
            feelings = repo.get_all(patient_id)

    if not feelings:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No Feelings found",
        )

    return feelings


@router.put(
    "/api/feelings/{id}", response_model=UpdateFeelingsResponse | HttpError
)
async def update_feelings(
    id: int,
    feelings_info: FeelingsIn,
    current_user: dict = Depends(authenticator.try_get_current_account_data),
    repo: FeelingsQueries = Depends(),
):
    if current_user:
        id = current_user.get("id")
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Not authenticated",
        )

    if not id:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not authenticated",
        )

    updated = repo.update_feelings(id, feelings_info)

    if updated is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Feelings not found",
        )
    elif updated:
        return UpdateFeelingsResponse(
            success=True, message="Feelings updated successfully"
        )
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Failed to update Feelings",
        )


@router.delete("/api/feelings/{id}", response_model=bool)
async def delete(
    id: int,
    current_user: dict = Depends(authenticator.try_get_current_account_data),
    repo: FeelingsQueries = Depends(),
) -> bool:
    if current_user:
        role = current_user.get("role")

    if role == "hcp":
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="You cannot delete a patient's feelings as an HCP.",
        )
    elif role == "patient":
        patient_id = current_user.get("id")
        return repo.delete(id, patient_id)
