from fastapi import APIRouter, Depends, HTTPException, status
from authenticator import authenticator

from typing import List, Union, Optional
from models.appointments import AppointmentIn, UpdateAptResponse, HttpError
from queries.appointments import (
    Error,
    AppointmentOut,
    AppointmentQueries,
)

router = APIRouter()


@router.post("/api/appointments", response_model=Union[AppointmentOut, Error])
async def book_appointment(
    apt: AppointmentIn,
    current_user: dict = Depends(authenticator.try_get_current_account_data),
    repo: AppointmentQueries = Depends(),
):
    if current_user:
        role = current_user.get("role")

    if role == "patient":
        patient_id = current_user.get("id")
        return repo.create(apt, patient_id)
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Only patients can book appointments",
        )


@router.get(
    "/api/appointments/{id}/",
    response_model=Optional[AppointmentOut] | HttpError,
)
async def get_appointment(
    id: int,
    current_user: dict = Depends(authenticator.try_get_current_account_data),
    repo: AppointmentQueries = Depends(),
) -> AppointmentOut:
    if current_user:
        role = current_user.get("role")
        appointment = None

        if role == "hcp":
            hcp_id = current_user.get("id")
            patient_id = None
            appointment = repo.get_one(id, patient_id, hcp_id)
        elif role == "patient":
            patient_id = current_user.get("id")
            hcp_id = None
            appointment = repo.get_one(id, patient_id)

    if appointment is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="You do not have an appointment with a id of {}.".format(
                id
            ),
        )

    return appointment


@router.get(
    "/api/appointments", response_model=List[AppointmentOut] | HttpError
)
async def get_all_appointments(
    repo: AppointmentQueries = Depends(),
    current_user: dict = Depends(authenticator.get_current_account_data),
):
    if current_user:
        role = current_user.get("role")

        if role == "patient":
            patient_id = current_user.get("id")
            hcp_id = None
            appointments = repo.get_all(patient_id)

        elif role == "hcp":
            hcp_id = current_user.get("id")
            patient_id = None
            appointments = repo.get_all(patient_id, hcp_id)

        if appointments:
            return appointments
        else:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="You do not have any appointments scheduled.",
            )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You are not logged in.",
        )


@router.put(
    "/api/appointments/{id}", response_model=UpdateAptResponse | HttpError
)
async def update_appointment(
    id: int,
    apt_info: AppointmentIn,
    current_user: dict = Depends(authenticator.try_get_current_account_data),
    repo: AppointmentQueries = Depends(),
):
    if current_user:
        role = current_user.get("role")

    updated = repo.update_appointment(id, apt_info)

    if role == "patient":
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Contact your HCP to update your appointment",
        )
    elif role == "hcp":
        if updated:
            return UpdateAptResponse(
                success=True, message="Appointment updated successfully"
            )
        else:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Failed to update appointment",
            )
    if updated is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Appointment not found",
        )


@router.delete("/api/appointments/{id}", response_model=bool)
async def cancel_appointment(
    id: int,
    repo: AppointmentQueries = Depends(),
) -> bool:
    return repo.delete(id)
