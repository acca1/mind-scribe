from typing import List
from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from models.accounts import (
    AccountToken,
    AccountIn,
    AccountOut,
    AccountForm,
    DuplicateAccountError,
)
from authenticator import authenticator
from pydantic import BaseModel
from queries.accounts import AccountQueries


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = repo.create(info, hashed_password=hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.get("/api/hcp-accounts", response_model=List[AccountOut])
async def get_hcp_accounts(
    account_queries: AccountQueries = Depends(),
) -> List[AccountOut]:
    hcp_accounts = account_queries.getAll(role="hcp")

    if not hcp_accounts:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No hcp accounts found",
        )
    return hcp_accounts


@router.get("/api/patient-accounts", response_model=List[AccountOut])
async def get_patient_accounts(
    account_queries: AccountQueries = Depends(),
) -> List[AccountOut]:
    patient_accounts = account_queries.getAll(role="patient")

    if not patient_accounts:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No patient accounts found",
        )
    return patient_accounts
