from fastapi import FastAPI
from authenticator import authenticator
from routers import accounts, appointments, feelings
from fastapi.middleware.cors import CORSMiddleware
import os

app = FastAPI()

app.include_router(authenticator.router, tags=["Auth"])
app.include_router(accounts.router, tags=["Auth"])
app.include_router(appointments.router, tags=["Appointments"])
app.include_router(feelings.router, tags=["Feelings"])

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:5173")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
