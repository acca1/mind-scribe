from models.accounts import AccountOutWithHashedPassword, AccountOut
from queries.pool import pool
from typing import List


class AccountQueries:
    def create(self, data, hashed_password) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                params = [
                    data.profile_picture,
                    data.first_name,
                    data.last_name,
                    data.birthdate,
                    data.username,
                    hashed_password,
                    data.address,
                    data.company,
                    data.role,
                ]
                db.execute(
                    """
                    INSERT INTO accounts(
                        profile_picture,
                        first_name,
                        last_name,
                        birthdate,
                        username,
                        hashed_password,
                        address,
                        company,
                        role
                    )
                    VALUES
                        (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                    RETURNING
                        id,
                        profile_picture,
                        first_name,
                        last_name,
                        birthdate,
                        username,
                        hashed_password,
                        address,
                        company,
                        role
                    ;
                    """,
                    params,
                )

                record = None
                row = db.fetchone()
                if row is not None:
                    record = {}
                    for i, col in enumerate(db.description):
                        record[col[0]] = row[i]
                return AccountOutWithHashedPassword(**record)

    def get(self, username: str) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT *
                    FROM accounts
                    WHERE username = %s;
                    """,
                    (username,),
                )
                try:
                    record = None
                    row = db.fetchone()
                    if row is not None:
                        record = {}
                        for i, col in enumerate(db.description):
                            record[col[0]] = row[i]
                    return AccountOutWithHashedPassword(**record)
                except Exception:
                    return {"message": "Account not found"}

    def getAll(self, role: str) -> List[AccountOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT
                        id,
                        profile_picture,
                        first_name,
                        last_name,
                        birthdate,
                        username,
                        address,
                        company,
                        role
                    FROM accounts
                    WHERE role = %s;
                    """,
                    (role,),
                )
                try:
                    records = []
                    rows = db.fetchall()
                    for row in rows:
                        record = dict(
                            zip([col[0] for col in db.description], row)
                        )
                        records.append(AccountOut(**record))
                    return records
                except Exception:
                    return False
