from queries.pool import pool
from typing import List, Union, Optional
from models.feelings import FeelingsOut, HttpError


class FeelingsQueries:
    def create(self, feelings, patient=None):
        feelings_out = FeelingsOut(
            id=0,
            patient=patient,
            emoji=feelings.emoji,
            description=feelings.description,
            name=FeelingsOut.extract_name_from_filename(feelings.emoji),
        )
        with pool.connection() as conn:
            with conn.cursor() as db:
                params = [
                    feelings_out.patient,
                    feelings_out.emoji,
                    feelings_out.description,
                    feelings_out.date,
                    feelings_out.name,
                ]
                db.execute(
                    """
                    INSERT INTO feelings
                        (
                            patient,
                            emoji,
                            description,
                            date,
                            name
                        )
                    VALUES
                        (%s, %s, %s, %s, %s)
                    RETURNING
                        id,
                        patient,
                        emoji,
                        name,
                        description,
                        date
                    ;
                    """,
                    params,
                )

                record = None
                row = db.fetchone()
                if row is not None:
                    record = {}
                    for i, col in enumerate(db.description):
                        record[col[0]] = row[i]
                return FeelingsOut(**record)

    def get_one(self, id: int, patient_id) -> Optional[FeelingsOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            SELECT
                                id,
                                patient,
                                emoji,
                                name,
                                description,
                                date
                            FROM feelings
                            WHERE id = %s
                            AND patient = %s
                            """,
                        [id, patient_id],
                    )
                    record = None
                    row = db.fetchone()
                    if row is not None:
                        record = {}
                        for i, col in enumerate(db.description):
                            record[col[0]] = row[i]
                    return FeelingsOut(**record)
        except Exception:
            return None

    def get_all(self, patient_id) -> Union[HttpError, List[FeelingsOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            patient,
                            emoji,
                            name,
                            description,
                            date
                        FROM feelings
                        WHERE patient = %s
                        ORDER BY date;
                        """,
                        [patient_id],
                    )
                    return [
                        FeelingsOut(
                            id=feelings[0],
                            patient=feelings[1],
                            emoji=feelings[2],
                            name=feelings[3],
                            description=feelings[4],
                            date=feelings[5],
                        )
                        for feelings in db
                    ]
        except Exception:
            return None

    def update_feelings(self, id, feelings_info):
        with pool.connection() as conn:
            with conn.cursor() as db:
                params = []
                set_clause = []
                if feelings_info.emoji:
                    params.append(feelings_info.emoji)
                    set_clause.append("emoji = %s")
                if feelings_info.description:
                    params.append(feelings_info.description)
                    set_clause.append("description = %s")
                params.append(id)

                if set_clause:
                    set_clause = ", ".join(set_clause)
                    db.execute(
                        f"""
                        UPDATE feelings
                        SET
                            {set_clause}
                        WHERE id = %s;
                        """,
                        params,
                    )

                    if db.rowcount > 0:
                        return True
                    else:
                        return None
                else:
                    return True

    def delete(self, id: int, patient_id) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM feelings
                        WHERE id = %s
                        AND patient = %s
                        """,
                        [id, patient_id],
                    )
                    return True
        except Exception:
            return False
