from pydantic import BaseModel
from queries.pool import pool
from typing import List, Union, Optional
from models.appointments import AppointmentOut


class Error(BaseModel):
    message: str


class AppointmentQueries:
    def create(self, apt, patient_id):
        with pool.connection() as conn:
            with conn.cursor() as db:
                params = [
                    patient_id,
                    apt.hcp_id,
                    apt.appt_date,
                    apt.appt_time,
                    apt.appt_reason,
                    apt.status,
                ]
                db.execute(
                    """
                    INSERT INTO appointments
                        (
                            patient_id,
                            hcp_id,
                            appt_date,
                            appt_time,
                            appt_reason,
                            status
                        )
                    VALUES
                        (%s, %s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    params,
                )

                db.execute(
                    """
                    SELECT
                        a.id,
                        a.patient_id,
                        a.hcp_id,
                        a.appt_date,
                        a.appt_time,
                        a.appt_reason,
                        a.status,
                        patient.first_name AS patient_first_name,
                        patient.last_name AS patient_last_name,
                        hcp.first_name AS hcp_first_name,
                        hcp.last_name AS hcp_last_name,
                        patient.birthdate AS patient_birthdate
                    FROM appointments a
                    JOIN accounts patient ON a.patient_id = patient.id
                    JOIN accounts hcp ON a.hcp_id = hcp.id
                    WHERE a.id = %s;
                    """,
                    [db.fetchone()[0]],
                )

                record = None
                row = db.fetchone()
                if row is not None:
                    record = {}
                    for i, col in enumerate(db.description):
                        record[col[0]] = row[i]
                return AppointmentOut(**record)

    def get_one(
        self, id: int, patient_id=None, hcp_id=None
    ) -> Optional[AppointmentOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    if patient_id or hcp_id:
                        db.execute(
                            """
                            SELECT
                                id,
                                patient_id,
                                hcp_id,
                                appt_date,
                                appt_time,
                                appt_reason,
                                status
                            FROM appointments
                            WHERE id = %s
                            AND (patient_id = %s OR hcp_id = %s)
                            """,
                            [id, patient_id, hcp_id],
                        )
                    record = None
                    row = db.fetchone()
                    if row is not None:
                        record = {}
                        for i, col in enumerate(db.description):
                            record[col[0]] = row[i]
                    return AppointmentOut(**record)
        except Exception:
            return None

    def get_all(
        self, patient_id=None, hcp_id=None
    ) -> Union[Error, List[AppointmentOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    if patient_id:
                        db.execute(
                            """
                            SELECT
                                a.id,
                                a.patient_id,
                                a.hcp_id,
                                a.appt_date,
                                a.appt_time,
                                a.appt_reason,
                                a.status,
                                patient.first_name AS patient_first_name,
                                patient.last_name AS patient_last_name,
                                hcp.first_name AS hcp_first_name,
                                hcp.last_name AS hcp_last_name,
                                patient.birthdate AS patient_birthdate
                            FROM appointments a
                            JOIN accounts patient ON a.patient_id = patient.id
                            JOIN accounts hcp ON a.hcp_id = hcp.id
                            WHERE a.patient_id = %s
                            ORDER BY a.appt_date;
                            """,
                            [patient_id],
                        )
                    elif hcp_id:
                        db.execute(
                            """
                            SELECT
                                a.id,
                                a.patient_id,
                                a.hcp_id,
                                a.appt_date,
                                a.appt_time,
                                a.appt_reason,
                                a.status,
                                patient.first_name AS patient_first_name,
                                patient.last_name AS patient_last_name,
                                hcp.first_name AS hcp_first_name,
                                hcp.last_name AS hcp_last_name,
                                patient.birthdate AS patient_birthdate
                            FROM appointments a
                            JOIN accounts patient ON a.patient_id = patient.id
                            JOIN accounts hcp ON a.hcp_id = hcp.id
                            WHERE a.hcp_id = %s
                            ORDER BY a.appt_date;
                            """,
                            [hcp_id],
                        )
                    return [
                        AppointmentOut(
                            id=apt[0],
                            patient_id=apt[1],
                            hcp_id=apt[2],
                            appt_date=apt[3],
                            appt_time=apt[4],
                            appt_reason=apt[5],
                            status=apt[6],
                            patient_first_name=apt[7],
                            patient_last_name=apt[8],
                            hcp_first_name=apt[9],
                            hcp_last_name=apt[10],
                            patient_birthdate=apt[11],
                        )
                        for apt in db
                    ]
        except Exception:
            return False

    def update_appointment(self, id, apt_info):
        with pool.connection() as conn:
            with conn.cursor() as db:
                params = []
                set_clause = []
                if apt_info.appt_date:
                    params.append(apt_info.appt_date)
                    set_clause.append("appt_date = %s")
                if apt_info.appt_time:
                    params.append(apt_info.appt_time)
                    set_clause.append("appt_time = %s")
                if apt_info.appt_reason:
                    params.append(apt_info.appt_reason)
                    set_clause.append("appt_reason = %s")
                if apt_info.status:
                    params.append(apt_info.status)
                    set_clause.append("status = %s")

                params.append(id)

                if set_clause:
                    set_clause = ", ".join(set_clause)
                    db.execute(
                        f"""
                        UPDATE appointments
                        SET
                            {set_clause}
                        WHERE id = %s;
                        """,
                        params,
                    )

                    if db.rowcount > 0:
                        return True
                    else:
                        return None
                else:
                    return True

    def delete(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM appointments
                        WHERE id = %s
                        """,
                        [id],
                    )
                    return True
        except Exception:
            return False
