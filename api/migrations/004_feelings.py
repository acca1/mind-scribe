steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE feelings  (
            id SERIAL PRIMARY KEY NOT NULL,
            patient INT REFERENCES accounts(id) NOT NULL,
            emoji TEXT NOT NULL,
            name TEXT NOT NULL,
            description TEXT NOT NULL,
            date TEXT NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE feelings;
        """,
    ],
]
