steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE accounts  (
            id SERIAL PRIMARY KEY NOT NULL,
            profile_picture TEXT NOT NULL,
            first_name TEXT NOT NULL,
            last_name TEXT NOT NULL,
            birthdate TEXT NOT NULL,
            username TEXT NOT NULL,
            hashed_password VARCHAR(255) NOT NULL,
            address TEXT,
            company TEXT,
            role TEXT NOT NULL DEFAULT 'user' CHECK (
                role IN ('hcp', 'patient')
            )
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE accounts;
        """,
    ],
]
