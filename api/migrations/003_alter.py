steps = [
    [
        # "Up" SQL statement
        """ALTER TABLE accounts
        ADD COLUMN apt_id INT REFERENCES appointments(id);
        """,
        # "Down" SQL statement
        """
        ALTER TABLE accounts
        DROP COLUMN apt_id;
        """,
    ]
]
