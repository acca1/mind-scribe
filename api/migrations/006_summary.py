steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE summaries (
            id SERIAL PRIMARY KEY NOT NULL,
            feeling_id INT REFERENCES feelings(id),
            patient_id INT REFERENCES accounts(id) NOT NULL,
            date_created TEXT,
            summary TEXT NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE summaries;
        """,
    ]
]
