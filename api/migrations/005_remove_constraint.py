steps = [
    [
        # "Up" SQL statement
        """
        ALTER TABLE appointments
        ALTER COLUMN patient_id DROP NOT NULL,
        ALTER COLUMN hcp_id DROP NOT NULL;
        """,
        # "Down" SQL statement
        """
        ALTER TABLE appointments
        ALTER COLUMN patient_id SET NOT NULL,
        ALTER COLUMN hcp_id SET NOT NULL;
        """,
    ]
]
