steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE appointments (
            id SERIAL PRIMARY KEY,
            patient_id INT REFERENCES accounts(id) NOT NULL,
            hcp_id INT REFERENCES accounts(id) NOT NULL,
            appt_date TEXT NOT NULL,
            appt_time TEXT NOT NULL,
            appt_reason TEXT NOT NULL,
            status TEXT NOT NULL DEFAULT 'requested' CHECK (
                status IN ('requested', 'scheduled', 'canceled', 'completed')
            )
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE appointments;
        """,
    ],
]
