<div align="center">

# Mindscribe
_Mental Health Tracking made easy_

</div>

[![Gitlab Release](https://img.shields.io/badge/version-1.0-green)](https://gitlab.com/drizzymakeplays/cardealer)


# Features
- Ability for patients to add and track feelings, and connect with doctors through appointments, with RESTful API.
- Ability for doctors to accept appointments, view appointmenmts, cancel appointmenmts and see a list of patients that they have had appointmenmts with, through use of a RESTful API.

# Requirements
[<img src="https://www.svgrepo.com/show/331370/docker.svg" alt="docker" width="100" height="100">](https://docs.docker.com/get-docker/) Docker
[<img src="https://www.svgrepo.com/show/452210/git.svg" alt="Git" width="100" height="100">](https://git-scm.com/) Git


# Project Installation
1. Fork repository and clone using git.
```bash
git clone https://gitlab.com/***YourNameHere***/onthebooks.git
```
2. Change to the directory that was just created
```bash
cd mindscribe
```
3. Create a .env file in the root directory(onthebooks)
```bash
mkdir .env
```
4. Add these environment variables to the .env file
```bash
SIGNING_KEY=enter a random string of charaters without spaces
PGADMIN_EMAIL=your.email@domain.com
PGADMIN_PASSWORD=yourpassword
```
5. Create a .env file
```bash
mkdir .env
```
6. Add this environment variable to the .env file you just created to connect the backend to the frontend
```bash
VITE_API_HOST = 'http://localhost:8000'
```
```
7. Create both Docker volumes, build and run your docker container
```bash
docker volume create mindscribe-data
docker volume create pg-admin
docker-compose build
docker-compose up
```

### App paths
* Use http://localhost:5173/ to connect to the app.
* Use http://localhost:8000/docs to connect to Fastapi Swagger for testing the endpoints and documentation.
* Use http://localhost:8082/ to connect to pg-admin database for running queries and viewing tables.

### Installing python dependencies locally

In order for VSCode's built in code completion and intelligence to
work correctly, it needs the dependencies from the requirements.txt file
installed. this is done inside docker during the build, but not in your local the workspace.

So you will need to create a virtual environment and pip install the requirements.

1. From inside the `api` folder:

```bash
python -m venv .venv
```

2. Then activate the virtual environment

```bash
source .venv/bin/activate
```

3. And finally install the dependencies

```bash
pip install -r requirements.txt
```
4. Close VSCode and reopen

# Design
### Database Diagram
![alt text](Diagram.png)
![alt text](Models.png)

```sql
CREATE TABLE accounts (
    id SERIAL PRIMARY KEY NOT NULL,
    profile_picture TEXT NOT NULL,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    birthdate TEXT NOT NULL,
    username TEXT NOT NULL,
    hashed_password VARCHAR(255) NOT NULL,
    address TEXT,
    company TEXT,
    role TEXT NOT NULL DEFAULT 'user' CHECK (
        role IN ('hcp', 'patient')
    )
);

CREATE TABLE appointments (
    id SERIAL PRIMARY KEY,
    patient_id INT REFERENCES accounts(id) NOT NULL,
    hcp_id INT REFERENCES accounts(id) NOT NULL,
    appt_date TEXT NOT NULL,
    appt_time TEXT NOT NULL,
    appt_reason TEXT NOT NULL,
    status TEXT NOT NULL DEFAULT 'requested' CHECK (
        status IN ('requested', 'scheduled', 'canceled', 'completed')
    )
);

CREATE TABLE feelings (
    id SERIAL PRIMARY KEY NOT NULL,
    patient INT REFERENCES accounts(id) NOT NULL,
    emoji TEXT NOT NULL,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    date TEXT NOT NULL
);


# API URL:
http://localhost:5173/

# Authentication
| Behavior | Method | URL |
| --- | --- | --- |
| Register a new account | GET | `/token` |
| Log in to a account | POST | `/token` |
| Log out of account | DELETE | `/token` |
| Get token | GET | `/token` |
|Get hcp accounts | GET | `/api/hcp-accounts` |
| Get patient accounts | GET | `/api/patient-accounts` |

### Authentication JSON

```bash
# GET
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": 0,
    "username": "string",
    "first_name": "string",
    "last_name": "string",
    "birthdate": "string",
    "profile_picture": "string",
    "address": "string",
    "company": "string",
    "role": "string"
  }
}

# GET
{
  "id": 0,
  "patient_first_name": "string",
  "patient_last_name": "string",
  "patient_birthdate": "string",
  "hcp_first_name": "string",
  "hcp_last_name": "string",
  "patient_id": 0,
  "hcp_id": 0,
  "appt_date": "2024-02-07",
  "appt_time": "string",
  "appt_reason": "string",
  "status": "string"
}

# POST
{
  "username": "string",
  "password": "string",

}

# GET
[
  {
    "id": 2,
    "username": "Doc",
    "first_name": "Doctor",
    "last_name": "Strange",
    "birthdate": "1977-10-20",
    "profile_picture": "string",
    "address": "string",
    "company": "string",
    "role": "hcp"
  }
]

# GET
[
  {
    "id": 0,
    "username": "string",
    "first_name": "string",
    "last_name": "string",
    "birthdate": "string",
    "profile_picture": "string",
    "address": "string",
    "company": "string",
    "role": "string"
  }
]

# DELETE
True
```

# Appointments
| Behavior | Method | URL |
| --- | --- | --- |
| Get one appointment  | GET | `/api/appointments/{patient_id} or {hcp_id}` |
| Get all appointments| GET | `/api/appointments`|
| Book an appointment  | POST | `/api/appointments` |
| Update appointment  | PUT | `/api/appointments/{appt_id}` |
| cancel appointment  | DELETE | `/api/appointments/{appt_id}` |
### Appointments JSON

```bash
# GET
[
  {
    "id": 0,
    "patient_first_name": "string",
    "patient_last_name": "string",
    "patient_birthdate": "string",
    "hcp_first_name": "string",
    "hcp_last_name": "string",
    "patient_id": 0,
    "hcp_id": 0,
    "appt_date": "2024-02-07",
    "appt_time": "string",
    "appt_reason": "string",
    "status": "string"
  }
]

# GET
{
  "id": 0,
  "patient_first_name": "string",
  "patient_last_name": "string",
  "patient_birthdate": "string",
  "hcp_first_name": "string",
  "hcp_last_name": "string",
  "patient_id": 0,
  "hcp_id": 0,
  "appt_date": "2024-02-07",
  "appt_time": "string",
  "appt_reason": "string",
  "status": "string"
}

# POST
{
  "hcp_id": 2,
  "appt_date": "02-7-2023",
  "appt_time": "11:00am",
  "appt_reason": "sad boy vibes",
  "status": "requested"
}

# PUT
{
  "hcp_id": 0,
  "appt_date": "string",
  "appt_time": "string",
  "appt_reason": "string",
  "status": "requested"
}

# DELETE
True
```

# Feelings
| Behavior | Method | URL |
| --- | --- | --- |
| Get all feelings  | GET | `/api/feelings` |
| Get one feeling  | GET | `/api/feelings/{patient_id}` |
| Add a feeling  | POST | `/api/feelings` |
| Update a feeling  | PUT | `/api/feelings/{feeling_id}` |
| Delete a feeling  | DELETE | `/api/feelings/{feeling_id}` |

### Feelings JSON

```bash
# GET
[
  {
    "id": 0,
    "patient": 0,
    "name": "string",
    "date": "string",
    "description": "string",
    "emoji": "string"
  }
]

# GET
{
  "id": 0,
  "patient": 0,
  "name": "string",
  "date": "string",
  "description": "string",
  "emoji": "string"
}

# POST
{
  "emoji": "string",
  "description": "string"
}

# PUT
{
  "emoji": "string",
  "description": "string"
}

# DELETE
True
```


### Built With
| [<img src="https://www.svgrepo.com/show/331370/docker.svg" alt="docker" width="200" height=150px>](https://docs.docker.com/get-docker/) Docker | [<img src="https://github.com/drizzymakeplays/onthebooks/assets/118935604/4461ab69-77ce-4f01-8f97-d2c7278ef30a" width=200px height=150px>](https://fastapi.tiangolo.com/) FastApi | [<img src="https://www.svgrepo.com/show/374167/vite.svg" alt="Vite.js" width="200" height=150px>](https://vitejs.dev/) Vite.js | [<img src="https://www.svgrepo.com/show/452091/python.svg" alt="Vite.js" width="200" height=150px>](https://www.python.org/) Python | [<img src="https://github.com/drizzymakeplays/onthebooks/assets/118935604/b4a75de8-4afc-4520-b742-eb9a564ae1f6" width=200px height=150px>](https://getbootstrap.com/) BootStrap |
| ----- | --- | ----- | --- | ----- |
