import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import './css/signup.css'
import { Link } from 'react-router-dom'
import { useSignupMutation, useGetTokenQuery } from '../app/apiSlice'

function SignUpForm() {
    const navigate = useNavigate()
    const { data: account, refetch } = useGetTokenQuery()
    const [signup, { isSuccess }] =
        useSignupMutation()
    const [username, setUsername] = useState('')
    const [first_name, setFirst_name] = useState('')
    const [last_name, setLast_name] = useState('')
    const [birthdate, setBirthdate] = useState('')
    const [password, setPassword] = useState('')
    const [company, setCompany] = useState('')
    const [address, setAddress] = useState('')
    const [role, setRole] = useState('patient')
    const [profile_picture, setProfile_picture] = useState('')


    useEffect(() => {
        if (isSuccess && account?.account) {
            const { role} = account.account
            if (role === 'hcp') {
                navigate(`/patient/list`)
            } else if (role === 'patient') {
                navigate(`/feelings`)
            }
        }
    }, [isSuccess, account, navigate])

    const handleSubmit = async (e) => {
        e.preventDefault()

        const dateParts = birthdate.split('-')
        const formattedBirthdate = `${dateParts[1]}-${dateParts[2]}-${dateParts[0]}`
        const accountData = {
            username,
            first_name,
            last_name,
            birthdate: formattedBirthdate,
            profile_picture,
            password,
            company,
            address,
            role,
        }

        try {
            await signup(accountData).unwrap()
            await refetch()
        } catch (err) {
            console.error('signup failed', err)
        }
    }

    return (
        <div className="row sign-up-page">
            <img className="MS-logo" alt="MS logo" src=".\public\MS-logo.png" />
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4 signup-form">
                    <h1 className="sign-up-font">Signup</h1>
                    <form onSubmit={handleSubmit} className="my-form">
                        <div
                            className="mb-3 row"
                            style={{ position: 'relative' }}
                        >
                            <div className="col-6">
                                <label
                                    className="sign-up-label firstname-label"
                                    htmlFor="first_Name"
                                    style={{
                                        color: '#47e6d3',
                                        fontSize: '1.3em',
                                    }}
                                >
                                    First Name
                                </label>
                                <input
                                    placeholder="First Name"
                                    required
                                    type="text"
                                    name="first_Name"
                                    className="form-control firstname-field"
                                    value={first_name}
                                    onChange={(e) =>
                                        setFirst_name(e.target.value)
                                    }
                                />
                            </div>
                            <div className="col-6">
                                <label
                                    className="sign-up-label lastname-label"
                                    htmlFor="last_name"
                                    style={{
                                        color: '#47e6d3',
                                        fontSize: '1.3em',
                                    }}
                                >
                                    Last Name
                                </label>
                                <input
                                    placeholder="Last Name"
                                    required
                                    type="text"
                                    name="last_name"
                                    className="form-control lastname-field"
                                    value={last_name}
                                    onChange={(e) =>
                                        setLast_name(e.target.value)
                                    }
                                />
                            </div>
                        </div>
                        <label
                            className="sign-up-label"
                            htmlFor="username"
                            style={{
                                color: '#47e6d3',
                                fontSize: '1.3em',
                                position: 'relative',
                                top: '-5px',
                                left: '225px',
                            }}
                        >
                            Username:
                        </label>
                        <div className="mb-3">
                            <input
                                placeholder="username"
                                required
                                type="text"
                                name="username"
                                id="username"
                                className="form-control"
                                style={{ position: 'relative', top: '-5px' }}
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                            />
                        </div>
                        <label
                            className="sign-up-label"
                            htmlFor="birthdate"
                            style={{
                                color: '#47e6d3',
                                fontSize: '1.3em',
                                position: 'relative',
                                top: '-25px',
                                left: '225px',
                            }}
                        >
                            Birthdate:
                        </label>
                        <div className="mb-3">
                            <input
                                required
                                type="date"
                                name="name"
                                className="form-control"
                                style={{ position: 'relative', top: '-25px' }}
                                value={birthdate}
                                onChange={(e) => setBirthdate(e.target.value)}
                            />
                        </div>
                        <label
                            className="sign-up-label"
                            htmlFor="birthdate"
                            style={{
                                color: '#47e6d3',
                                fontSize: '1.3em',
                                position: 'relative',
                                top: '-45px',
                                left: '225px',
                            }}
                        >
                            Role:
                        </label>
                        <div className="mb-3">
                            <select
                                placeholder="Patient"
                                required
                                name="role"
                                id="role"
                                className="form-control"
                                style={{ position: 'relative', top: '-45px' }}
                                value={role}
                                onChange={(e) => setRole(e.target.value)}
                            >
                                <option value="patient">Patient</option>
                                <option value="hcp">
                                    Health Care Provider
                                </option>
                            </select>
                        </div>
                        <label
                            className="sign-up-label"
                            htmlFor="password"
                            style={{
                                color: '#47e6d3',
                                fontSize: '1.3em',
                                position: 'relative',
                                top: '-65px',
                                left: '225px',
                            }}
                        >
                            Password:
                        </label>
                        <div className="mb-3">
                            <input
                                placeholder="Password"
                                required
                                type="password"
                                name="name"
                                className="form-control"
                                style={{ position: 'relative', top: '-65px' }}
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>
                        <label
                            className="sign-up-label"
                            htmlFor="profile_picture"
                            style={{
                                color: '#47e6d3',
                                fontSize: '1.3em',
                                position: 'relative',
                                top: '-85px',
                                left: '225px',
                            }}
                        >
                            Profile Picture:
                        </label>
                        <div className="mb-3">
                            <input
                                placeholder=""
                                type="file"
                                name="profile_picture"
                                id="profile_picture"
                                className="form-control"
                                style={{ position: 'relative', top: '-85px' }}
                                value={profile_picture}
                                onChange={(e) =>
                                    setProfile_picture(e.target.value)
                                }
                            />
                        </div>
                        {role === 'hcp' && (
                            <>
                                <label
                                    className="sign-up-label"
                                    htmlFor="company"
                                    style={{
                                        color: '#47e6d3',
                                        fontSize: '1.3em',
                                        position: 'relative',
                                        top: '-105px',
                                        left: '225px',
                                    }}
                                >
                                    Company:
                                </label>
                                <div className="mb-3">
                                    <input
                                        placeholder="company name ..."
                                        required
                                        type="text"
                                        name="company"
                                        id="company"
                                        className="form-control"
                                        style={{
                                            position: 'relative',
                                            top: '-105px',
                                        }}
                                        value={company}
                                        onChange={(e) =>
                                            setCompany(e.target.value)
                                        }
                                    />
                                </div>
                                <label
                                    className="sign-up-label"
                                    htmlFor="address"
                                    style={{
                                        color: '#47e6d3',
                                        fontSize: '1.3em',
                                        position: 'relative',
                                        top: '-125px',
                                        left: '225px',
                                    }}
                                >
                                    Company Address:
                                </label>
                                <div className="mb-3">
                                    <input
                                        placeholder="Company address ..."
                                        required
                                        type="text"
                                        name="address"
                                        id="address"
                                        className="form-control"
                                        style={{
                                            position: 'relative',
                                            top: '-125px',
                                        }}
                                        value={address}
                                        onChange={(e) =>
                                            setAddress(e.target.value)
                                        }
                                    />
                                </div>
                            </>
                        )}
                        <button
                            type="submit"
                            className="btn signup-btn signup-button"
                        >
                            Signup
                        </button>
                    </form>
                    <div className="login-tool-tip">
                        <div className="overlap-group">
                            <div className="have-account">Have an account?</div>
                            <Link to="/login" className="login-text">
                                Login!
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SignUpForm
