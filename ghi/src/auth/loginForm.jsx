import { useState, useEffect } from 'react'
import './css/login.css'
import { useNavigate, Link } from 'react-router-dom'
import { useLoginMutation, useGetTokenQuery } from '../app/apiSlice'

function LoginForm() {
    const navigate = useNavigate()
    const [login, { isLoading, isSuccess }] = useLoginMutation()
    const { data: account, refetch } = useGetTokenQuery()
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [errorMessage, setErrorMessage] = useState('')

    useEffect(() => {
        if (isSuccess && account?.account) {
            const { role} = account.account
            if (role === 'hcp') {
                navigate(`/patient/list`)
            } else if (role === 'patient') {
                navigate(`/feelings`)
            }
        }
    }, [isSuccess, account, navigate])

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            await login({ email: username, password }).unwrap()
            await refetch()
        } catch (err) {
            setErrorMessage('Incorrect username or password. Please try again.')
        }
    }

    return (
        <div className="row login-page">
            <img className="MS-logo" alt="MS logo" src=".\public\MS-logo.png" />
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4 login-form">
                    <h1 className="login-font">Login</h1>
                    <form onSubmit={handleSubmit}>
                        {errorMessage && <div className="alert alert-danger" role="alert">
                            {errorMessage}
                        </div>}
                        <label
                            className="login-label"
                            htmlFor="username"
                            style={{ color: '#47e6d3', fontSize: '1.3em' }}
                        >
                            Username:
                        </label>
                        <div className="mb-3">
                            <input
                                placeholder="Username"
                                required
                                type="text"
                                id="username"
                                className="form-control"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                            />
                        </div>

                        <label
                            className="login-label"
                            htmlFor="password"
                            style={{ color: '#47e6d3', fontSize: '1.3em' }}
                        >
                            Password:
                        </label>
                        <div className="mb-3">
                            <input
                                placeholder="Password"
                                required
                                type="password"
                                id="password"
                                className="form-control"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>
                        <button
                            type="submit"
                            className="btn login-btn"
                            disabled={isLoading}
                        >
                            Login
                        </button>
                        <div className="sign-up-tool-tip">
                            <div className="overlap-group">
                                <div className="no-account">
                                    No account?
                                </div>
                                <Link to="/signup" className="signup-text">
                                    Signup!
                                </Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default LoginForm
