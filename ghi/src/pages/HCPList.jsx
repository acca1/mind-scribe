import PatientNav from '../components/PatientNav'
import { useGetHcpsQuery } from '../app/apiSlice'
import SiteFooter from '../components/Footer'

const HCPList = () => {
    const { data: hcpData, isLoading } = useGetHcpsQuery()

    if (isLoading) {
        return (
            <>
                <div>
                    <PatientNav />
                </div>
                <div className="col ms-1 mb-0 pt-5">Loading...</div>
            </>
        )
    }

    if (!hcpData || hcpData?.length === 0) {
        return (
            <>
                <div>
                    <PatientNav />
                </div>
                <div className="row ms-1 mb-0 pt-5">
                    <div className="mt-3 pt-2">
                        No health care providers found.
                    </div>
                </div>
            </>
        )
    }

    return (
        <>
            <div>
                <PatientNav />
            </div>
            <div className="mt-5" style={{ alignContent: 'center' }}>
                <h1 style={{ textAlign: 'center' }}>
                    Available Health Care Professionals
                </h1>
                <div className="row row-cols-1 row-cols-md-4 g-5 ms-5 mt-5">
                    {hcpData.map((hcp) => (
                        <div key={hcp.id} className="col">
                            <div className="card h-100">
                                <img
                                    src="/public/profilePictures/profilepic_1.jpg"
                                    className="card-img-top"
                                    alt="..."
                                />
                                <div className="card-body">
                                    <h5 className="card-title">
                                        Name:{' '}
                                        {hcp.first_name + ' ' + hcp.last_name}
                                    </h5>
                                    <p className="card-text">
                                        Company: {hcp.company}
                                        <br />
                                        Address: {hcp.address}
                                    </p>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <div
                className="site-footer"
                style={{ position: 'fixed', bottom: '0px', width: '100%' }}
            >
                <SiteFooter />
            </div>
        </>
    )
}

export default HCPList
