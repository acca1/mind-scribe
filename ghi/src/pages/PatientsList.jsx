import { useState, useEffect } from 'react'
import HCPNav from '../components/HCPNav'
import { useGetAppointmentsQuery} from '../app/apiSlice';
import SiteFooter from '../components/Footer'


function PatientsList() {
    const { data: PatientData, isLoading } = useGetAppointmentsQuery()
    const [searchName, setSearchName] = useState('')
    const [filteredPatients, setFilteredPatients] = useState([])
    const uniquePatientNames = new Set()

    const convertNameFormat = (inputName) => {
        const Name = inputName
        return Name
    }

    useEffect(() => {
        if (PatientData) {
            setFilteredPatients(PatientData)
        }
    }, [PatientData])


    const handleNameChange = (e) => {
        const inputName = e.target.value
        setSearchName(inputName)


        if (inputName) {
            const formattedInputName = convertNameFormat(inputName)
            const filtered = PatientData.filter((patient) => {
                const fullName = `${patient.patient_first_name} ${patient.patient_last_name}`
                return fullName.toLowerCase().includes(formattedInputName.toLowerCase())
            });
            setFilteredPatients(filtered);
        } else {
            setFilteredPatients(PatientData);
        }
    }

    if (isLoading) {
        return (
            <>
                <div>
                    <HCPNav />
                </div>
                <div className="col ms-1 mb-0 pt-5">Loading...</div>
            </>
        )
    }

    if (!PatientData || PatientData?.length === 0) {
        return (
            <>
                <div>
                    <HCPNav />
                </div>
                <div className="row ms-1 mb-0 pt-5">
                    <div className="mt-3 pt-2">No patients found.</div>
                </div>
            </>
        )
    }

    return (
        <>
            <div>
                <HCPNav />
            </div>
            <div className="mt-5" style={{ alignContent: 'center' }}>
                <h1 style={{ textAlign: 'center' }}>Patients List</h1>
                <div className="input-group search-bar">
                    <input
                        className="input"
                        type="text"
                        value={searchName}
                        onChange={handleNameChange}
                        placeholder="Enter Name"
                    />
                </div>
               <div className="row row-cols-1 row-cols-md-4 g-5 ms-5 mt-5">
                    {filteredPatients.map((patient) => {
                        const { id, patient_first_name, patient_last_name, patient_birthdate } = patient;
                        const fullName = `${patient_first_name} ${patient_last_name}`;

                        if (!uniquePatientNames.has(fullName)) {
                        uniquePatientNames.add(fullName);
                        return (
                            <div key={id} className="col">
                                <div className="card h-100">
                                    <img
                                        src="/public/profilePictures/patient_1.jpg"
                                        className="card-img-top"
                                        alt="..."
                                    />
                                    <div className="card-body">
                                        <h5 className="card-title">
                                            Name: {fullName}
                                        </h5>
                                        <p className="card-text">
                                            Birthdate: {patient_birthdate}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        )
                        }
                        return null;
                    })}
                </div>
            </div>
            <div
                className="site-footer"
                style={{ position: 'fixed', bottom: '0px', width: '100%' }}
            >
                <SiteFooter />
            </div>
        </>
    )
}


export default PatientsList;
