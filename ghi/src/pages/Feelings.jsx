import { useState, useEffect } from 'react'
import PatientNav from '../components/PatientNav'
import { useGetFeelingsQuery } from '../app/apiSlice'
import SiteFooter from '../components/Footer'
import '../css/Feelings.css'
import { Link } from 'react-router-dom'

function Feelings() {
    const [searchDate, setSearchDate] = useState('')
    const [filteredFeelings, setFilteredFeelings] = useState([])
    const { data: feelingsData, isLoading} = useGetFeelingsQuery()

    const convertDateFormat = (inputDate) => {
        const dateParts = inputDate.split('-')
        const year = dateParts[0]
        const month = String(parseInt(dateParts[1], 10)).padStart(2, '0')
        const day = String(parseInt(dateParts[2], 10)).padStart(2, '0')
        return `${month}-${day}-${year}`
    }

    useEffect(() => {
        if (feelingsData) {
            setFilteredFeelings(feelingsData)
        }
    }, [feelingsData])

    const handleDateChange = (e) => {
        const inputDate = e.target.value
        setSearchDate(inputDate)

        if (inputDate) {
            const formattedInputDate = convertDateFormat(inputDate)
            const filtered = feelingsData.filter((feeling) => {
                return feeling.date === formattedInputDate
            })
            setFilteredFeelings(filtered)
        } else {
            setFilteredFeelings(feelingsData)
        }
    }

    if (isLoading) {
        return <div>Loading...</div>
    }

   return (
       <>
           <div className="my-nav">
               <PatientNav />
           </div>
           <h1 className="text-center feelings-text">Feelings</h1>
           <div colSpan="7" className="input-group search-bar">
               <input
                   className="input"
                   type="date"
                   value={searchDate}
                   onChange={handleDateChange}
                   placeholder="Enter Date"
               />
               <Link to="/feelings/create">
                   <button
                       type="button"
                       className="btn btn-outline-success"
                       style={{ marginLeft: '2em' }}
                   >
                       Add a Feeling
                   </button>
               </Link>
           </div>
           <div className="container" style={{ width: '100%', marginBottom: '15%' }}>
               <div className={`row ${filteredFeelings.length >= 2 ? 'row-cols-4' : 'row-cols-1'} justify-content-center`}>
                   {filteredFeelings.length > 0 ? (
                       filteredFeelings.map((feeling) => (
                           <div
                               key={feeling.id}
                               className="card text-center mb-2 ms-5 mt-2 shadow rounded"
                           >
                                <div className="row g-0">
                                    <div className="col-6">
                                        <img
                                            src={`/emojis/${feeling.emoji}`}
                                            className="img-fluid rounded-start"
                                            alt="..."
                                            style={{
                                                height: '100px',
                                                width: '100px',
                                            }}
                                        />
                                    </div>
                                    <div className="col-12">
                                        <div className="card-body">
                                            <h5 className="card-title">
                                                Date: {feeling.date}
                                            </h5>
                                            <p className="card-text">
                                                Description:{' '}
                                                {feeling.description}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                           </div>
                       ))
                   ) : (
                       <div className="col-12 text-center">
                           <p>No feelings to show.</p>
                       </div>
                   )}
               </div>
           </div>
           <div
               className="site-footer"
               style={{ position: "fixed", bottom: "0%", width: '100%', marginTop: '2em'}}
           >
               <SiteFooter />
           </div>
       </>
   )
}

export default Feelings
