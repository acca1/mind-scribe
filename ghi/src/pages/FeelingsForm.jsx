import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import PatientNav from '../components/PatientNav'
import '../css/FeelingsForm.css'
import { useCreateFeelingsMutation, useGetFeelingsQuery } from '../app/apiSlice'
import SiteFooter from '../components/Footer'

function FeelingsForm() {
    const navigate = useNavigate()
    const [create, { isSuccess }] = useCreateFeelingsMutation()
    const [emoji, setEmoji] = useState('')
    const { refetch: refetchFeelings } = useGetFeelingsQuery()
    const [description, setDescription] = useState('')

    const emojis = [
        {
            src: '/public/Emojis/angry.png',
            alt: 'Angry',
            value: 'angry.png',
        },
        {
            src: '/public/Emojis/frustrated.png',
            alt: 'Frustrated',
            value: 'frustrated.png',
        },
        {
            src: '/public/Emojis/happy.png',
            alt: 'Happy',
            value: 'happy.png',
        },
        {
            src: '/public/Emojis/annoyed.png',
            alt: 'Annoyed',
            value: 'annoyed.png',
        },
        {
            src: '/public/Emojis/content.png',
            alt: 'Content',
            value: 'content.png',
        },
        {
            src: '/public/Emojis/excited.png',
            alt: 'Excited',
            value: 'excited.png',
        },
        {
            src: '/public/Emojis/fear.png',
            alt: 'Fearful',
            value: 'fear.png',
        },
        {
            src: '/public/Emojis/hopeful.png',
            alt: 'Hopeful',
            value: 'hopeful.png',
        },
        {
            src: '/public/Emojis/lonely.png',
            alt: 'Lonely',
            value: 'lonely.png',
        },
        {
            src: '/public/Emojis/sad.png',
            alt: 'Sad',
            value: 'sad.png',
        },
    ]

    useEffect(() => {
        if (isSuccess) {
            navigate(`/feelings`)
        }
    })

    const handleSubmit = async (e) => {
        e.preventDefault()
        const feelingData = {
            emoji,
            description,
        }

        try {
            await create(feelingData).unwrap()
            refetchFeelings()
        } catch (err) {
            console.error('Failed to create Feeling', err)
        }
    }

    return (
        <>
            <div>
                <PatientNav />
            </div>
            <div
                className="text-center"
                style={{ position: 'relative', top: '60px' }}
            >
                <h1 className="tag">How are you feeling today?</h1>
                <h5 className="tag">
                    Select an emoji that best fits your mood!
                </h5>
            </div>
            <div
                className="container"
                style={{
                    alignContent: 'center',
                    position: 'relative',
                    top: '155px',
                }}
            >
                <form onSubmit={handleSubmit}>
                    <div className="image-selection">
                        {emojis.map((emojiItem, index) => (
                            <label
                                key={index}
                                className={`emoji-label ${
                                    emoji === emojiItem.value ? 'selected' : ''
                                }`}
                                style={{
                                    position: 'relative',
                                    display: 'inline-block',
                                    textAlign: 'center',
                                    margin: '10px',
                                    bottom: '70px',
                                }}
                            >
                                <input
                                    type="radio"
                                    name="selectedEmoji"
                                    value={emojiItem.value}
                                    onChange={() => setEmoji(emojiItem.value)}
                                    className="visually-hidden"
                                    checked={emoji === emojiItem.value}
                                />
                                <img
                                    src={emojiItem.src}
                                    alt={emojiItem.alt}
                                    style={{
                                        height: '200px',
                                        width: '200px',
                                    }}
                                    onClick={() => setEmoji(emojiItem.value)}
                                />
                                <div
                                    className="emoji-name"
                                    style={{ marginTop: '15px' }}
                                >
                                    {emojiItem.alt}
                                </div>
                            </label>
                        ))}
                    </div>
                    <div
                        className="input-group description"
                        style={{
                            position: 'relative',
                            bottom: '45px',
                            right: '370px',
                        }}
                    >
                        <textarea
                            className="form-control"
                            placeholder="Take note of your feelings here..."
                            maxLength="100"
                            style={{
                                position: 'relative',
                                left: '450px',
                                top: '-15px',
                                height: '100px',
                                width: '400px',
                            }}
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                        ></textarea>
                        <div>
                            <button
                                className="btn btn-outline-success"
                                type="submit"
                                style={{
                                    position: 'relative',
                                    top: '110px',
                                    width: '200px',
                                    right: '140px',
                                }}
                            >
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div
                className="site-footer"
                style={{ position: 'fixed', bottom: '0%', width: '100%' }}
            >
                <SiteFooter />
            </div>
        </>
    )
}

export default FeelingsForm
