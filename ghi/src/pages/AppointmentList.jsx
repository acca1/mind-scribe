import PatientNav from '../components/PatientNav'
import { Link } from 'react-router-dom'
import { useEffect, useState } from 'react'
import {
    useGetAppointmentsQuery,
    useGetTokenQuery,
    useUpdateAppointmentStatusMutation,
} from '../app/apiSlice'
import '../components/Nav.css'
import '../css/AppointmentList.css'
import SiteFooter from '../components/Footer'
import HCPNav from '../components/HCPNav'

const AppointmentList = () => {
    const [updateAppointmentStatus] = useUpdateAppointmentStatusMutation()
    const { data: aptData, isLoading } = useGetAppointmentsQuery()
    const [filteredAppointments, setFilteredAppointments] = useState([])
    const { data: tokenData } = useGetTokenQuery()
    const [startDate, setStartDate] = useState('')
    const [endDate, setEndDate] = useState('')

    const handleCancel = async (aptId) => {
        try {
            await updateAppointmentStatus({
                id: aptId,
                status: 'canceled',
            })
        } catch (error) {
            console.error('Error canceling appointment:', error)
        }
    }

    const handleSchedule = async (aptId) => {
        try {
            await updateAppointmentStatus({
                id: aptId,
                status: 'scheduled',
            })
        } catch (error) {
            console.error('Error scheduling appointment:', error)
        }
    }

    useEffect(() => {
        if (aptData) {
            setFilteredAppointments(aptData)
        }
    }, [aptData])

    const convertDateFormat = (inputDate) => {
        const dateParts = inputDate.split('-')
        const year = dateParts[0]
        const month = String(parseInt(dateParts[1], 10)).padStart(2, '0')
        const day = String(parseInt(dateParts[2], 10)).padStart(2, '0')
        return `${month}-${day}-${year}`
    }

    const handleEndDateChange = (e) => {
    setEndDate(e.target.value);
}

const handleStartDateChange = (e) => {
    setStartDate(e.target.value);
}

const handleSearch = () => {
    if (startDate && endDate) {
        const start = new Date(startDate)
        const end = new Date(endDate)
        const filtered = aptData
            ? aptData.filter((appointment) => {
                    const apptDate = new Date(appointment.appt_date)
                    return apptDate >= start && apptDate <= end
                })
            : []
        setFilteredAppointments(filtered)
    } else {
        setFilteredAppointments(aptData || [])
    }
}

    const sortedAppointments = filteredAppointments
        .map((appointment) => ({
            ...appointment,
            status:
                appointment.status.toLowerCase() === 'requested'
                    ? 'requested'
                    : appointment.status.toLowerCase() === 'scheduled'
                    ? 'scheduled'
                    : 'cancelled',
        }))
        .sort((a, b) => {
            const statusOrder = { requested: 0, scheduled: 1, cancelled: 2 }
            return statusOrder[a.status] - statusOrder[b.status]
        })

    if (isLoading) {
        return (
            <>
                <div>
                    <PatientNav />
                </div>
                <div className="col ms-1 mb-0 pt-5">Loading...</div>
            </>
        )
    }

    const role = tokenData?.account?.role

    return (
        <>
            <div>
                {role === 'patient' ? (
                    <div className="my-nav">
                        <PatientNav />
                    </div>
                ) : (
                    <div className="my-nav">
                        <HCPNav />
                    </div>
                )}
                <h1 className="text-center mt-5 appointments">Appointments</h1>
                <div style={{ marginTop: '3rem' }}>
                    <div className="container d-flex justify-content-between align-items-center">
                        <div className="d-flex">
                            <div className="date-input date-field">
                                <label htmlFor="startDate">Start Date:</label>
                                <input
                                    type="date"
                                    id="startDate"
                                    value={startDate}
                                    onChange={handleStartDateChange}
                                />
                            </div>
                            <div className="date-input date-field">
                                <label htmlFor="endDate">End Date:</label>
                                <input
                                    type="date"
                                    id="endDate"
                                    value={endDate}
                                    onChange={handleEndDateChange}
                                />
                            </div>
                            <button
                                type="button"
                                className="btn btn-outline-secondary search-button"
                                onClick={handleSearch}
                            >
                                Search
                            </button>
                        </div>
                    </div>
                    <div
                        className="row ms-1 mb-0 apt-container"
                        style={{ marginTop: '3rem' }}
                    >
                        <table className="table table-dark table-striped mt-3 pt-2">
                            <thead>
                                <tr className="">
                                    {role === 'hcp' ? (
                                        <>
                                            <th scope="col">
                                                {"Patient's First Name"}
                                            </th>
                                            <th scope="col">
                                                {"Patient's Last Name"}
                                            </th>
                                        </>
                                    ) : role === 'patient' ? (
                                        <>
                                            <th scope="col">
                                                {"HCP's First Name"}
                                            </th>
                                            <th scope="col">{"HCP's Last Name"}</th>
                                        </>
                                    ) : null}
                                    <th scope="col">Appointment Date</th>
                                    <th scope="col">Appointment Time</th>
                                    <th scope="col">Reason</th>
                                    <th scope="col">Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {aptData && aptData.length > 0 ? (
                                    sortedAppointments.map((apt) => (
                                        <tr key={`${apt.id}-${apt.appt_time}`}>
                                            {role === 'hcp' && aptData ? (
                                                <>
                                                    <td>
                                                        {apt.patient_first_name}
                                                    </td>
                                                    <td>
                                                        {apt.patient_last_name}
                                                    </td>
                                                </>
                                            ) : role === 'patient' &&
                                              aptData ? (
                                                <>
                                                    <td>
                                                        {apt.hcp_first_name}
                                                    </td>
                                                    <td>{apt.hcp_last_name}</td>
                                                </>
                                            ) : null}

                                            <td>
                                                {convertDateFormat(
                                                    apt.appt_date
                                                )}
                                            </td>
                                            <td>{apt.appt_time}</td>
                                            <td>{apt.appt_reason}</td>
                                            <td>{apt.status}</td>
                                            <td>
                                                {apt.status === 'scheduled' ? (
                                                    <button
                                                        type="button"
                                                        className="btn btn-sm btn-danger"
                                                        style={{
                                                            transition:
                                                                'background-color 0.3s, color 0.3s',
                                                            backgroundColor:
                                                                '#d24e4e',
                                                            color: '#fff',
                                                        }}
                                                        onClick={() =>
                                                            handleCancel(apt.id)
                                                        }
                                                    >
                                                        Cancel
                                                    </button>
                                                ) : apt.status ===
                                                  'cancelled' ? null : (
                                                    <>
                                                        <button
                                                            type="button"
                                                            className="btn btn-sm btn-danger cancel-button"
                                                            style={{
                                                                transition:
                                                                    'background-color 0.3s, color 0.3s',
                                                                backgroundColor:
                                                                    '#d24e4e',
                                                                color: '#fff',
                                                            }}
                                                            onClick={() =>
                                                                handleCancel(
                                                                    apt.id
                                                                )
                                                            }
                                                        >
                                                            Cancel
                                                        </button>

                                                        {role !== 'patient' ? (
                                                            <button
                                                                type="button"
                                                                className="btn btn-sm btn-success schedule-button"
                                                                style={{
                                                                    transition:
                                                                        'background-color 0.3s, color 0.3s',
                                                                    backgroundColor:
                                                                        '#5ebd9a',
                                                                    color: '#fff',
                                                                }}
                                                                onClick={() =>
                                                                    handleSchedule(
                                                                        apt.id
                                                                    )
                                                                }
                                                            >
                                                                Schedule
                                                            </button>
                                                        ) : null}
                                                    </>
                                                )}
                                            </td>
                                        </tr>
                                    ))
                                ) : (
                                    <tr>
                                        <td colSpan="8" className="text-center">
                                            No appointments found.
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {role !== 'hcp' && (
                <Link className="nav-link" to="/appointments/create">
                    <button className="btn btn-outline-success create-button">
                        Create Appointment
                    </button>
                </Link>
            )}
            <div
                className="site-footer"
                style={{ position: 'fixed', bottom: '0px', width: '100%' }}
            >
                <SiteFooter />
            </div>
        </>
    )
}

export default AppointmentList
