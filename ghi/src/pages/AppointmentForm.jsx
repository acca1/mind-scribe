import PatientNav from '../components/PatientNav'
import '../css/AppointmentForm.css'
import { useNavigate } from 'react-router-dom'
import {
    useGetHcpsQuery,
    useCreateAppointmentMutation,
    useGetTokenQuery,
    useGetAppointmentsQuery
} from '../app/apiSlice'
import { useEffect, useState } from 'react'

function AppointmentForm() {
    const navigate = useNavigate()
    const {data: hcps, error: hcpsError, isLoading: hcpsLoading, } = useGetHcpsQuery()
    const {data: tokenData} = useGetTokenQuery()
    const { refetch: refetchAppointments } = useGetAppointmentsQuery();
    const [hcpList, setHcpList] = useState([])
    const [selectedProviderId, setSelectedProviderId] = useState("1")
    const [selectedDate, setSelectedDate] = useState('')
    const [selectedTime, setSelectedTime] = useState('')
    const [selectedReason, setSelectedReason] = useState('')

    useEffect(() => {
        if (!hcpsLoading && !hcpsError && hcps) {
            setHcpList(hcps)
        }
    }, [hcpsLoading, hcpsError, hcps])

    const [createAppointmentMutation] = useCreateAppointmentMutation()

     const convertDateFormat = (inputDate) => {
         const dateParts = inputDate.split('-')
         const year = dateParts[0]
         const month = String(parseInt(dateParts[1], 10)).padStart(2, '0')
         const day = String(parseInt(dateParts[2], 10)).padStart(2, '0')
         return `${month}-${day}-${year}`
     }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const patientId = tokenData?.patient_id
            const formattedDate = convertDateFormat(selectedDate)

            await createAppointmentMutation({
                patient_id: patientId,
                hcp_id: selectedProviderId,
                appt_date: formattedDate,
                appt_time: selectedTime,
                appt_reason: selectedReason,
            })

            refetchAppointments()

            navigate('/appointments')
        } catch (error) {
            console.error('Failed to create appointment', error)
        }
    }

    return (
        <>
            <PatientNav />
            <div className="row create-appointment">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4 outer">
                        <form className="login-form" onSubmit={handleSubmit}>
                            <h1
                                className="appointment-font"
                                style={{ color: 'white', textAlign: 'center' }}
                            >
                                Create Appointment
                            </h1>
                            <label
                                className="login-label"
                                htmlFor="providerName"
                                style={{ color: '#47e6d3', fontSize: '1.3em' }}
                            >
                                {"Provider's name"}
                            </label>
                            <div className="mb-3">
                                <select
                                    aria-placeholder="Providers name"
                                    id="providerName"
                                    className="form-control"
                                    required
                                    value={selectedProviderId}
                                    onChange={(e) =>
                                        setSelectedProviderId(e.target.value)
                                    }
                                >
                                    <option>Select Provider</option>
                                    {hcpList.map((hcp) => (
                                        <option key={hcp.id} value={hcp.id}>
                                            {`${hcp.first_name} ${hcp.last_name}`}
                                        </option>
                                    ))}
                                </select>
                            </div>

                            <label
                                className="login-label"
                                htmlFor="date"
                                style={{ color: '#47e6d3', fontSize: '1.3em' }}
                            >
                                Date
                            </label>
                            <div className="mb-3">
                                <input
                                    type="date"
                                    className="form-control"
                                    value={selectedDate}
                                    onChange={(e) =>
                                        setSelectedDate(e.target.value)
                                    }
                                />
                            </div>

                            <label
                                className="login-label"
                                htmlFor="time"
                                style={{ color: '#47e6d3', fontSize: '1.3em' }}
                            >
                                Time
                            </label>
                            <div className="mb-3">
                                <input
                                    type="time"
                                    id="time"
                                    className="form-control"
                                    required
                                    value={selectedTime}
                                    onChange={(e) =>
                                        setSelectedTime(e.target.value)
                                    }
                                />
                            </div>

                            <label
                                className="login-label"
                                htmlFor="reason"
                                style={{ color: '#47e6d3', fontSize: '1.3em' }}
                            >
                                Reason
                            </label>
                            <div className="mb-3">
                                <textarea
                                    id="reason"
                                    className="form-control"
                                    rows="4"
                                    required
                                    maxLength="75"
                                    value={selectedReason}
                                    onChange={(e) =>
                                        setSelectedReason(e.target.value)
                                    }
                                ></textarea>
                            </div>

                            <button
                                type="submit"
                                className="btn login-btn mb-3"
                            >
                                Create
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AppointmentForm
