import './footer.css';

function SiteFooter() {
    return (
        <>
            <footer className="footer">
                <div className="footer-content">
                    <p>
                        Contact Us <span>Extra data for Contact Us</span>
                    </p>
                    <p>
                        About <span>As software developer students for our final project we created a mental health app to help people track their feelings and connect with health care professonials. We hope you find it useful.
                        </span>
                    </p>
                    <p>
                        Privacy Policy{' '}
                        <span>Extra data for Privacy Policy</span>
                    </p>
                </div>
            </footer>
        </>
    )
}

export default SiteFooter;
