import { NavLink, useNavigate } from 'react-router-dom'
import { useEffect } from 'react'
import { useLogoutMutation, useGetTokenQuery } from '../app/apiSlice'
import './Nav.css'

function HCPNav() {
    const navigate = useNavigate()
    const [logout, { isSuccess }] = useLogoutMutation()
    const { data: tokenData, refetch } = useGetTokenQuery()

    useEffect(() => {
        if (isSuccess) {
            navigate('/')
        }
    }, [isSuccess, navigate])

    const handleClick = async (e) => {
        e.preventDefault()
        try {
            await logout().unwrap()
            await refetch()
        } catch (err) {
            console.error('Failed to logout', err)
        }
    }


    let hcpFullName = ''
    if (tokenData && tokenData.account) {
        hcpFullName = `${tokenData.account.first_name} ${tokenData.account.last_name}`
    }

    return (
        <nav className="navbar navbar-expand-lg my-nav">
            <div className="container-fluid">
                <NavLink className="navbar-brand" to="/patient/list">
                    <img
                        className="logo"
                        src="/MS-logo-nav.png"
                        alt="Mindscribe Logo"
                    />
                </NavLink>

                <div
                    className="collapse navbar-collapse"
                    id="navbarNavAltMarkup"
                >
                    <div className="navbar-nav">
                        <NavLink className="nav-link" to="/patient/list">
                            Home
                        </NavLink>
                        <NavLink className="nav-link" to="/appointments">
                            Appointments
                        </NavLink>
                        <NavLink
                            onClick={handleClick}
                            className="nav-link"
                            to="/logout"
                            style={{color: "whitesmoke"}}
                        >
                            Log Out
                        </NavLink>
                        <div className="patient-class">
                            <h2 className="patient-name">
                                Hello, {hcpFullName}
                            </h2>{' '}
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default HCPNav
