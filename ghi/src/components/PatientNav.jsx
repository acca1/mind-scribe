import { NavLink, useNavigate } from 'react-router-dom'
import { useEffect } from 'react'
import './Nav.css'
import { useGetTokenQuery, useLogoutMutation } from '../app/apiSlice'

function PatientNav() {
    const navigate = useNavigate()
    const [logout, { isSuccess }] = useLogoutMutation()
    const { data: tokenData, refetch } = useGetTokenQuery()

    useEffect(() => {
        if (isSuccess) {
            navigate('/')
        }
    }, [isSuccess, navigate])

    const handleClick = async (e) => {
        e.preventDefault()
        try {
            await logout().unwrap()
            await refetch()
        } catch (err) {
            console.error('Failed to logout', err)
        }
    }

    let patientFullName = ''
    if (tokenData && tokenData.account) {
        patientFullName = `${tokenData.account.first_name} ${tokenData.account.last_name}`
    }

    return (
        <nav className="navbar navbar-expand-lg my-nav">
            <div className="container-fluid">
                <NavLink className="navbar-brand" to="/feelings">
                    <img
                        className="logo"
                        src="/MS-logo-nav.png"
                        alt="Mindscribe Logo"
                    />
                </NavLink>

                <div
                    className="collapse navbar-collapse"
                    id="navbarNavAltMarkup"
                >
                    <div className="navbar-nav mt-4 my-nav-links">
                        <NavLink className="nav-link" to={`/feelings`}>
                            Home
                        </NavLink>
                        <NavLink className="nav-link" to="/HCP/list">
                            Find an HCP
                        </NavLink>
                        <NavLink className="nav-link" to="/appointments">
                            Appointments
                        </NavLink>
                        <NavLink onClick={handleClick} className="nav-link" style={{color: "whitesmoke"}}>
                            Log Out
                        </NavLink>
                        <div className="patient-class">
                            <h2 className="patient-name">
                                Hello, {patientFullName}
                            </h2>
                            {' '}
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}

export default PatientNav
