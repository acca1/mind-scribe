import { BrowserRouter, Routes, Route } from 'react-router-dom'
import './App.css'
import SignUpForm from './auth/SignUpForm'
import LoginForm from './auth/loginForm'
import MainPage from './MainPage'
import AppointmentForm from './pages/AppointmentForm'
import AppointmentList from './pages/AppointmentList'
import Feelings from './pages/Feelings'
import FeelingsForm from './pages/FeelingsForm'
import HCPList from './pages/HCPList'
import PatientsList from './pages/PatientsList'
import { AuthProvider } from '@galvanize-inc/jwtdown-for-react'

console.table(import.meta.env)

const API_HOST = import.meta.env.VITE_API_HOST

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

function App() {
    return (
        <BrowserRouter>
            <AuthProvider baseUrl={API_HOST}>
                <Routes>
                    <Route exact path="/" element={<MainPage />} />
                    <Route exact path="/signup" element={<SignUpForm />} />
                    <Route exact path="/login" element={<LoginForm />} />
                    <Route
                        exact
                        path="/appointments/create"
                        element={<AppointmentForm />}
                    />
                    <Route
                        exact
                        path="/appointments"
                        element={<AppointmentList />}
                    />
                    <Route exact path={'/feelings'} element={<Feelings />} />
                    <Route
                        exact
                        path={'/feelings/create'}
                        element={<FeelingsForm />}
                    />
                    <Route exact path="/HCP/list" element={<HCPList />} />
                    <Route
                        exact
                        path="/patient/list"
                        element={<PatientsList />}
                    />
                </Routes>
            </AuthProvider>
        </BrowserRouter>
    )
}

export default App
