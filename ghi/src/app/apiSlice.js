import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const mindscribeApi = createApi({
    reducerPath: 'authentication',
    tagTypes: ['Token', 'Appointments', 'Account'],
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_API_HOST,
    }),
    endpoints: (builder) => ({
        signup: builder.mutation({
            query: (accountData) => ({
                url: '/api/accounts/',
                method: 'post',
                body: accountData,
                credentials: 'include',
            }),
            invalidatesTags: (result) => {
                return (result && ['Account']) || []
            },
        }),
        login: builder.mutation({
            query: (info) => {
                let formData = null
                if (info instanceof HTMLElement) {
                    formData = new FormData(info)
                } else {
                    formData = new FormData()
                    formData.append('username', info.email)
                    formData.append('password', info.password)
                }
                return {
                    url: '/token',
                    method: 'post',
                    body: formData,
                    credentials: 'include',
                }
            },
            invalidatesTags: (result) => {
                return (result && ['Account']) || []
            },
        }),
        getToken: builder.query({
            query: () => ({
                url: '/token',
                credentials: 'include',
            }),
            providesTags: ['Token'],
        }),
        logout: builder.mutation({
            query: () => ({
                url: '/token',
                method: 'delete',
                credentials: 'include',
            }),
            invalidatesTags: ['Account'],
        }),
        createAppointment: builder.mutation({
            query: (newAppointment) => ({
                url: '/api/appointments',
                method: 'post',
                credentials: 'include',
                body: newAppointment,
            }),
        }),
        getAppointments: builder.query({
            query: () => ({
                url: '/api/appointments',
                method: 'get',
                credentials: 'include',
            }),
            providesTags: ['Appointments'],
        }),
        updateAppointmentStatus: builder.mutation({
            query: ({ id, status }) => ({
                url: `/api/appointments/${id}`,
                method: 'put',
                credentials: 'include',
                body: { status },
            }),
            invalidatesTags: ['Appointments'],
        }),
        getHcps: builder.query({
            query: () => ({
                url: '/api/hcp-accounts',
                method: 'get',
                credentials: 'include',
            }),
        }),
        createFeelings: builder.mutation({
            query: (feelingData) => ({
                url: `/api/feelings`,
                method: 'post',
                credentials: 'include',
                body: feelingData,
            }),
        }),
        getFeelings: builder.query({
            query: () => ({
                url: `/api/feelings/`,
                method: 'get',
                credentials: 'include',
            }),
        }),
    }),
})

export const {
    useSignupMutation,
    useLoginMutation,
    useGetTokenQuery,
    useLogoutMutation,
    useCreateAppointmentMutation,
    useGetAppointmentsQuery,
    useUpdateAppointmentStatusMutation,
    useGetHcpsQuery,
    useCreateFeelingsMutation,
    useGetFeelingsQuery,
} = mindscribeApi
