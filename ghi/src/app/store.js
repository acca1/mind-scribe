import { configureStore } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import queryReducer from './queryslice'
import { mindscribeApi } from './apiSlice'


export const store = configureStore({
    reducer: {
        query: queryReducer,
        [mindscribeApi.reducerPath]: mindscribeApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(mindscribeApi.middleware),
})

setupListeners(store.dispatch)
