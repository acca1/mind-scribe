import './mainPage.css'
import { Link } from 'react-router-dom'

function MainPage() {
    return (
        <div className="sign-up-login">
            <div className="sign-up-login">
                <img className="logo-landing" src="/MS-logo.png" alt="" />
                <div className="col-lg-6 mx-auto tag-line">
                    <p className="lead mb-4">
                        MindScribe is a journaling app that allows you to track
                        your thoughts and feelings in a safe and secure
                        environment.
                    </p>
                </div>

                <div className="d-grid gap-2 d-sm-flex justify-content-sm-center mb-5">
                    <Link to="/login">
                        <button
                            type="button"
                            className="btn btn-primary btn-lg px-4 me-sm-3 login-button"
                        >
                            Login
                        </button>
                    </Link>
                </div>
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-center mb-5">
                    <Link to="/signup">
                        <button
                            type="button"
                            className="btn btn-primary btn-lg px-4 me-sm-3 sign-up-button"
                        >
                            Signup
                        </button>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default MainPage
